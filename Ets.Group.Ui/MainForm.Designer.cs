﻿namespace Ets.Group.Ui
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeViewCatalogs = new System.Windows.Forms.TreeView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.gbCatalogLevel = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnInvertCollapseNodes = new System.Windows.Forms.Button();
            this.btnGenerateTreView = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.gbCatalogLevel.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeViewCatalogs
            // 
            this.treeViewCatalogs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewCatalogs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.treeViewCatalogs.Location = new System.Drawing.Point(3, 18);
            this.treeViewCatalogs.Margin = new System.Windows.Forms.Padding(10);
            this.treeViewCatalogs.Name = "treeViewCatalogs";
            this.treeViewCatalogs.Size = new System.Drawing.Size(1041, 472);
            this.treeViewCatalogs.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.gbCatalogLevel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.400722F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 92.59928F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1067, 554);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // gbCatalogLevel
            // 
            this.gbCatalogLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.gbCatalogLevel.Controls.Add(this.treeViewCatalogs);
            this.gbCatalogLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbCatalogLevel.Location = new System.Drawing.Point(10, 51);
            this.gbCatalogLevel.Margin = new System.Windows.Forms.Padding(10);
            this.gbCatalogLevel.Name = "gbCatalogLevel";
            this.gbCatalogLevel.Size = new System.Drawing.Size(1047, 493);
            this.gbCatalogLevel.TabIndex = 2;
            this.gbCatalogLevel.TabStop = false;
            this.gbCatalogLevel.Text = "Catalog Level..";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.btnInvertCollapseNodes, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnGenerateTreView, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1061, 35);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // btnInvertCollapseNodes
            // 
            this.btnInvertCollapseNodes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnInvertCollapseNodes.Location = new System.Drawing.Point(358, 5);
            this.btnInvertCollapseNodes.Margin = new System.Windows.Forms.Padding(5);
            this.btnInvertCollapseNodes.Name = "btnInvertCollapseNodes";
            this.btnInvertCollapseNodes.Size = new System.Drawing.Size(343, 25);
            this.btnInvertCollapseNodes.TabIndex = 1;
            this.btnInvertCollapseNodes.Text = "Инвертировать свертывание нод\r\n";
            this.btnInvertCollapseNodes.UseVisualStyleBackColor = true;
            // 
            // btnGenerateTreView
            // 
            this.btnGenerateTreView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnGenerateTreView.Location = new System.Drawing.Point(5, 5);
            this.btnGenerateTreView.Margin = new System.Windows.Forms.Padding(5);
            this.btnGenerateTreView.Name = "btnGenerateTreView";
            this.btnGenerateTreView.Size = new System.Drawing.Size(343, 25);
            this.btnGenerateTreView.TabIndex = 2;
            this.btnGenerateTreView.Text = "Обновить дерево";
            this.btnGenerateTreView.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnGenerateTreView.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Тестовое задание";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.gbCatalogLevel.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeViewCatalogs;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox gbCatalogLevel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnInvertCollapseNodes;
        private System.Windows.Forms.Button btnGenerateTreView;
    }
}