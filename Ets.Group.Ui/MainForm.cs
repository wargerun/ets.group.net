﻿using Ets.Group.Ui.ViewModels;
using System.Windows.Forms;

namespace Ets.Group.Ui
{
    public partial class MainForm : Form
    {
        private MainViewModel _viewModel;

        public MainForm()
        {
            InitializeComponent();
            InitialControlHandlers();
        }

        private void InitialControlHandlers()
        {
            _viewModel = new MainViewModel();
            _viewModel.SetTreeView(treeViewCatalogs);

            // registration events
            Load += (o, e) => _viewModel.MainWindowOnLoadedCommand.Execute(o);
            btnGenerateTreView.Click += (o, e) => _viewModel.GeneratedTreViewFromCatalogLevelsCommand.Execute(o);
            btnInvertCollapseNodes.Click += (o, e) => _viewModel.InvertCollapseNodesCommand.Execute(o);
        }
    }
}
