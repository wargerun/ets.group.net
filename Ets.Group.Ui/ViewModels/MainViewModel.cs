﻿using Ets.Group.Data;
using Ets.Group.Data.Db.Entity;
using Ets.Group.Ui.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Ets.Group.Data.Helpers;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Ets.Group.Data.Db.EtsGroupService;

namespace Ets.Group.Ui.ViewModels
{
    public class MainViewModel : BindableBase
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private TreeView _treeViewCatalogs;

        public ICommand InvertCollapseNodesCommand { get; }
        public ICommand MainWindowOnLoadedCommand { get; }
        public ICommand GeneratedTreViewFromCatalogLevelsCommand { get; }

        public MainViewModel()
        {
            RelayCommand generatedTreViewFromCatalogLevelsCommand = new RelayCommand(GeneratedTreViewFromCatalogLevels, () => TreeViewCatalogsIsExist());
            MainWindowOnLoadedCommand = generatedTreViewFromCatalogLevelsCommand;
            GeneratedTreViewFromCatalogLevelsCommand = generatedTreViewFromCatalogLevelsCommand;
            InvertCollapseNodesCommand = new RelayCommand(InvertCollapseNodes, () => TreeViewCatalogsIsExist());
        }

        private void GeneratedTreViewFromCatalogLevels()
        {
            try
            {
                logger.Info("Start GeneratedTreViewFromCatalogLevels");
                _treeViewCatalogs.Nodes.Clear();
                TransferCatalogLevels transferCatalogLevels = new TransferCatalogLevels(new EtsGroupServiceDb());
                List<CatalogLevel> catalogLevels = transferCatalogLevels.GetMigrationCatalogLevels();
                FillNode(catalogLevels, null);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                logger.Info("Finished GeneratedTreViewFromCatalogLevels");
            }
        }

        private bool TreeViewCatalogsIsExist() => !(_treeViewCatalogs is null);

        private void InvertCollapseNodes()
        {
            try
            {
                logger.Info("Start InvertCollapseNodes");
                InvertCollapseNodes(_treeViewCatalogs.Nodes);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                logger.Info("Finished InvertCollapseNodes");
            }
        }

        private void InvertCollapseNodes(TreeNodeCollection nodes)
        {
            foreach (TreeNode treeNode in nodes)
            {
                if (treeNode.Nodes != null)
                {
                    InvertCollapseNodes(treeNode.Nodes);
                }

                if (treeNode.IsExpanded)
                {
                    treeNode.Collapse();
                }
                else
                {
                    treeNode.Expand();
                }
            }
        }

        internal void SetTreeView(TreeView treeViewCatalogs) => _treeViewCatalogs = treeViewCatalogs;

        private void FillNode(List<CatalogLevel> items, TreeNode node)
        {
            int parentId = (int)(node.With(n => n.Tag) ?? 0);
            TreeNodeCollection nodesCollection = node.With(n => n.Nodes) ?? _treeViewCatalogs.Nodes;

            foreach (CatalogLevel item in items.Where(i => i.ParentId == parentId))
            {
                TreeNode newNode = nodesCollection.Add(item.Id.ToString(), item.Name);
                newNode.Tag = item.Id;

                FillNode(items, newNode);
            }
        }
    }
}
