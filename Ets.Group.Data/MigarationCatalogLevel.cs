﻿using Ets.Group.Data.Db.Entity;

namespace Ets.Group.Data
{
    internal class MigarationCatalogLevel : CatalogLevel
    {
        public string LinkId { get; set; }

        public override string ToString() => $"{base.ToString()} - LinkId: {LinkId}";
    }
}
