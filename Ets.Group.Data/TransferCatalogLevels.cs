﻿using Ets.Group.Data.Db.Entity;
using Ets.Group.Data.Db.EtsGroupService;
using System.Collections.Generic;
using System.Linq;

namespace Ets.Group.Data
{
    public class TransferCatalogLevels
    {
        private readonly IEtsGroupService _etsGroupService;

        public TransferCatalogLevels(IEtsGroupService etsGroupService) => _etsGroupService = etsGroupService;

        public List<CatalogLevel> GetMigrationCatalogLevels()
        {
            List<MigarationCatalogLevel> catalogLevels = new List<MigarationCatalogLevel>();
            _etsGroupService.CatalogLevelRepository.DeleteAll();
            
            FillCatalogsLevelFromCatalogs(catalogLevels, _etsGroupService.CatalogRepository.GetCatalogs());
            FillCatalogsLevelFromAggregates(catalogLevels, _etsGroupService.AggregateRepository.GetAggregates());
            FillCatalogsLevelFromModels(catalogLevels, _etsGroupService.ModelRepository.GetModels());

            return catalogLevels.OfType<CatalogLevel>().ToList();
        }

        private void FillCatalogsLevelFromModels(List<MigarationCatalogLevel> catalogLevels, IEnumerable<Model> models)
        {
            foreach (Model model in models)
            {
                MigarationCatalogLevel parentCatalogLevel = GetParentCatalogLevel(catalogLevels, $"{nameof(Aggregate)}{model.AggregateId}");
                if (parentCatalogLevel is null)
                    continue;

                MigarationCatalogLevel catalogLevel = new MigarationCatalogLevel()
                {
                    Name = model.Name,
                    ParentId = parentCatalogLevel.Id,
                    LinkId = model.GetUniqueId(),
                };

                PushAndLocalSaveCatalogLevel(catalogLevels, catalogLevel);
            }
        }

        private void FillCatalogsLevelFromAggregates(List<MigarationCatalogLevel> catalogLevels, IEnumerable<Aggregate> aggregates)
        {
            foreach (Aggregate aggregate in aggregates)
            {
                MigarationCatalogLevel parentCatalogLevel = GetParentCatalogLevel(catalogLevels, $"{nameof(Catalog)}{aggregate.CatalogId}");
                if (parentCatalogLevel is null)
                    continue;

                MigarationCatalogLevel catalogLevel = new MigarationCatalogLevel()
                {
                    Name = aggregate.Name,
                    ParentId = parentCatalogLevel.Id,
                    LinkId = aggregate.GetUniqueId(),
                };

                PushAndLocalSaveCatalogLevel(catalogLevels, catalogLevel);
            }
        }

        private static MigarationCatalogLevel GetParentCatalogLevel(List<MigarationCatalogLevel> catalogLevels, string parentLinkedId)
        {
            return catalogLevels.Find(cl => cl.LinkId == parentLinkedId);
        }

        private void FillCatalogsLevelFromCatalogs(ICollection<MigarationCatalogLevel> catalogLevels, IEnumerable<Catalog> catalogs)
        {
            foreach (Catalog catalog in catalogs)
            {
                MigarationCatalogLevel catalogLevel = new MigarationCatalogLevel()
                {
                    Name = catalog.Name,
                    LinkId = catalog.GetUniqueId(),
                };
                
                PushAndLocalSaveCatalogLevel(catalogLevels, catalogLevel);
            }
        }

        private void PushAndLocalSaveCatalogLevel(ICollection<MigarationCatalogLevel> catalogLevels, MigarationCatalogLevel catalogLevel)
        {
            CatalogLevel catalogLevelLocal = _etsGroupService.CatalogLevelRepository.Add(catalogLevel);
            catalogLevel.Id = catalogLevelLocal.Id;
            catalogLevels.Add(catalogLevel);
        }
    }
}
