﻿using System;

namespace Ets.Group.Data.Helpers
{
    public static class GenericExtensions
    {
        public static void AssertDisposing<T>(this T self, bool disposing) where T : IDisposable
        {
            if (disposing)
            {
                self.Dispose();
            }
        }  
        
        public static TOut With<TIn, TOut>(this TIn @in, Func<TIn, TOut> func) 
            where TIn : class
            where TOut : class
        {
            return @in is null ? null : func(@in);
        }
    }
}
