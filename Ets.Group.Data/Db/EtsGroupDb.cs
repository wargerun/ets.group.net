﻿using Ets.Group.Data.Db.Entity;
using Microsoft.EntityFrameworkCore;

namespace Ets.Group.Data.Db
{
    public class EtsGroupDb : DbContext
    {
        public EtsGroupDb()
        {
            // Database.SetInitializer(new CreateDatabaseIfNotExists<EtsGroupDb>());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Filename = local path data base
            optionsBuilder.UseSqlite(@"FileName=EtsGroupDb.sqllite");
        }

        public static EtsGroupDb GetDbContext(EtsGroupDb dbContext = null) => dbContext ?? new EtsGroupDb();

        public virtual DbSet<Catalog> Catalogs { get; set; }
        public virtual DbSet<Aggregate> Aggregates { get; set; }
        public virtual DbSet<Model> Models { get; set; }
        public virtual DbSet<CatalogLevel> CatalogLevels { get; set; }
    }
}
