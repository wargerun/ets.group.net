﻿namespace Ets.Group.Data.Db.Entity
{
    public class Model
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AggregateId { get; set; }

        public virtual Aggregate Aggregate { get; set; }

        internal string GetUniqueId() => $"{nameof(Model)}{Id}";
    }
}
