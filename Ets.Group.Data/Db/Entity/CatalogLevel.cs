﻿namespace Ets.Group.Data.Db.Entity
{
    public class CatalogLevel
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }

        public override string ToString() => $"{Id} - {ParentId} - {Name}";
    }
}
