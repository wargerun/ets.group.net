﻿using System.Diagnostics;

namespace Ets.Group.Data.Db.Entity
{
    [DebuggerDisplay("Name={Name}; Id={Id}")]
    public class Aggregate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CatalogId { get; set; }

        public virtual Catalog Catalog { get; set; }
        public virtual Model Model { get; set; }

        internal string GetUniqueId() => $"{nameof(Aggregate)}{Id}";
    }
}
