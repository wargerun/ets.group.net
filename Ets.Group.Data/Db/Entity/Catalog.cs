﻿using System.Diagnostics;

namespace Ets.Group.Data.Db.Entity
{                                      
    [DebuggerDisplay("Name={Name}; Id={Id}")]
    public class Catalog
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string GetUniqueId() => $"{nameof(Catalog)}{Id}";

        public virtual Aggregate Aggregate { get; set; }
    }
}
