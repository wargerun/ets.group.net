﻿using Ets.Group.Data.Db.EtsGroupService.DTO.AggregateService;
using Ets.Group.Data.Db.EtsGroupService.DTO.CatalogLevelService;
using Ets.Group.Data.Db.EtsGroupService.DTO.CatalogService;
using Ets.Group.Data.Db.EtsGroupService.DTO.ModelService;

namespace Ets.Group.Data.Db.EtsGroupService
{
    public class EtsGroupServiceDb : IEtsGroupService
    {                                           
        public EtsGroupServiceDb(EtsGroupDb etsGroupDb = null)
        {
            CatalogRepository = new CatalogRepository();
            CatalogLevelRepository = new CatalogLevelRepository();
            AggregateRepository = new AggregateRepository();
            ModelRepository = new ModelRepository();
        }

        public ICatalogRepository CatalogRepository { get; }

        public ICatalogLevelRepository CatalogLevelRepository { get; }

        public IAggregateRepository AggregateRepository { get; }

        public IModelRepository ModelRepository { get; }
    }
}
