﻿namespace Ets.Group.Data.Db.EtsGroupService.DTO
{
    internal abstract class EtsGroupDbRepositoryBase
    {   
        public EtsGroupDb EtsGroupDb { get; }

        protected EtsGroupDbRepositoryBase(EtsGroupDb etsGroupDb = null)
        {
            EtsGroupDb = EtsGroupDb.GetDbContext(etsGroupDb);
        }                                    
    }
}
