﻿using System.Collections.Generic;
using Ets.Group.Data.Db.Entity;

namespace Ets.Group.Data.Db.EtsGroupService.DTO.ModelService
{
    internal class ModelRepositoryMock : IModelRepository
    {
        public IEnumerable<Model> GetModels()
        {
            yield return new Model { Id = 1, Name = "A365", AggregateId = 1 };
            yield return new Model { Id = 2, Name = "M4566", AggregateId = 2 };
            yield return new Model { Id = 3, Name = "FG4511", AggregateId = 2 };
            yield return new Model { Id = 4, Name = "T45459", AggregateId = 3 };
        }
    }
}
