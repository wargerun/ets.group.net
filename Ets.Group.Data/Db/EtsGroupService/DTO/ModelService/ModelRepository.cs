﻿using System.Collections.Generic;
using System.Linq;
using Ets.Group.Data.Db.Entity;

namespace Ets.Group.Data.Db.EtsGroupService.DTO.ModelService
{
    internal class ModelRepository : EtsGroupDbRepositoryBase, IModelRepository
    {
        public ModelRepository(EtsGroupDb etsGroupDb = null) : base(etsGroupDb)
        {
        }

        public IEnumerable<Model> GetModels() => EtsGroupDb.Models.ToList();
    }
}
