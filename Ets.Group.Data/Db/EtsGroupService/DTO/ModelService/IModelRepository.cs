﻿using System.Collections.Generic;
using Ets.Group.Data.Db.Entity;

namespace Ets.Group.Data.Db.EtsGroupService.DTO.ModelService
{
    public interface IModelRepository
    {
        IEnumerable<Model> GetModels();
    }
}
