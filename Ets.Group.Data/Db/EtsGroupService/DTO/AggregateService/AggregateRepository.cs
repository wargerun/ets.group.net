﻿using System.Collections.Generic;
using System.Linq;
using Ets.Group.Data.Db.Entity;

namespace Ets.Group.Data.Db.EtsGroupService.DTO.AggregateService
{
    internal class AggregateRepository : EtsGroupDbRepositoryBase, IAggregateRepository
    {
        public AggregateRepository(EtsGroupDb etsGroupDb = null) : base(etsGroupDb)
        {
        }

        public IEnumerable<Aggregate> GetAggregates()
        {
            return EtsGroupDb.Aggregates.ToList();
        }
    }
}
