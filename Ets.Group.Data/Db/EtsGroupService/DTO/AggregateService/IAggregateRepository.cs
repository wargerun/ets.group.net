﻿using System.Collections.Generic;
using Ets.Group.Data.Db.Entity;

namespace Ets.Group.Data.Db.EtsGroupService.DTO.AggregateService
{
    public interface IAggregateRepository
    {
        IEnumerable<Aggregate> GetAggregates();
    }
}
