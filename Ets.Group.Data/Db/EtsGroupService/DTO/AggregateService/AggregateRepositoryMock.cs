﻿using System.Collections.Generic;
using Ets.Group.Data.Db.Entity;

namespace Ets.Group.Data.Db.EtsGroupService.DTO.AggregateService
{
    internal class AggregateRepositoryMock : IAggregateRepository
    {
        public IEnumerable<Aggregate> GetAggregates()
        {
            yield return new Aggregate { Id = 1, Name = "КПП", CatalogId = 1 };
            yield return new Aggregate { Id = 2, Name = "Двигатель", CatalogId = 2 };
            yield return new Aggregate { Id = 3, Name = "КПП", CatalogId = 2 };
        }
    }
}
