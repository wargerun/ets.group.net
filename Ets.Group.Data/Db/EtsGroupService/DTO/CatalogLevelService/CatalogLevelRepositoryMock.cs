﻿using Ets.Group.Data.Db.Entity;

namespace Ets.Group.Data.Db.EtsGroupService.DTO.CatalogLevelService
{
    internal class CatalogLevelRepositoryMock : ICatalogLevelRepository
    {
        private int idSequence = 0;

        public CatalogLevel Add(CatalogLevel catalogLevel)
        {
            catalogLevel.Id = idSequence++;
            return catalogLevel;
        }

        public void DeleteAll()
        {
            // Sector clear!
            // Roger that!
        }
    }
}
