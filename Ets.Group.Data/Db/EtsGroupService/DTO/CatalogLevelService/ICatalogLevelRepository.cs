﻿using Ets.Group.Data.Db.Entity;

namespace Ets.Group.Data.Db.EtsGroupService.DTO.CatalogLevelService
{
    public interface ICatalogLevelRepository
    {
        void DeleteAll();
        CatalogLevel Add(CatalogLevel catalogLevel);
    }
}
