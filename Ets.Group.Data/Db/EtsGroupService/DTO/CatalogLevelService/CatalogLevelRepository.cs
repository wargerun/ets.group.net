﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ets.Group.Data.Db.Entity;

namespace Ets.Group.Data.Db.EtsGroupService.DTO.CatalogLevelService
{
    internal class CatalogLevelRepository : EtsGroupDbRepositoryBase, ICatalogLevelRepository
    {
        public CatalogLevelRepository(EtsGroupDb etsGroupDb = null) : base(etsGroupDb)
        {
        }

        public CatalogLevel Add(CatalogLevel catalogLevel)
        {
            CatalogLevel localCatalogLevel = EtsGroupDb.CatalogLevels.Find(catalogLevel.Id);
            if (localCatalogLevel != null)
            {
                throw new InvalidOperationException($"CatalogLevel with id: {catalogLevel.Id}, named: {catalogLevel.Name} is exist !");
            }
            else
            {
                localCatalogLevel = EtsGroupDb.CatalogLevels.Add(catalogLevel)?.Entity;
            }  

            EtsGroupDb.SaveChanges();
            return localCatalogLevel;
        }

        public void DeleteAll()
        {
            List<CatalogLevel> catalogLevels = EtsGroupDb.CatalogLevels.ToList();
            EtsGroupDb.CatalogLevels.RemoveRange(catalogLevels);
            EtsGroupDb.SaveChanges();
        }
    }
}
