﻿using System.Collections.Generic;
using Ets.Group.Data.Db.Entity;

namespace Ets.Group.Data.Db.EtsGroupService.DTO.CatalogService
{
    internal class CatalogRepositoryMock : ICatalogRepository
    {
        public IEnumerable<Catalog> GetCatalogs()
        {
            yield return new Catalog { Id = 1, Name = "VOLVO" };
            yield return new Catalog { Id = 2, Name = "ER" };
        }
    }
}
