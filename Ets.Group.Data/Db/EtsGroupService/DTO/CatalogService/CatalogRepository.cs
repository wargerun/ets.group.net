﻿using System.Collections.Generic;
using System.Linq;
using Ets.Group.Data.Db.Entity;

namespace Ets.Group.Data.Db.EtsGroupService.DTO.CatalogService
{
    internal class CatalogRepository : EtsGroupDbRepositoryBase, ICatalogRepository
    {
        public CatalogRepository(EtsGroupDb etsGroupDb = null) : base(etsGroupDb)
        {
        }

        public IEnumerable<Catalog> GetCatalogs()
        {
            return EtsGroupDb.Catalogs.ToList();
        }
    }
}
