﻿using System.Collections.Generic;
using Ets.Group.Data.Db.Entity;

namespace Ets.Group.Data.Db.EtsGroupService.DTO.CatalogService
{
    public interface ICatalogRepository
    {
        IEnumerable<Catalog> GetCatalogs();
    }
}
