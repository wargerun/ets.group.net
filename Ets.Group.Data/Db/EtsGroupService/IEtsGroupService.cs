﻿using Ets.Group.Data.Db.EtsGroupService.DTO.AggregateService;
using Ets.Group.Data.Db.EtsGroupService.DTO.CatalogLevelService;
using Ets.Group.Data.Db.EtsGroupService.DTO.CatalogService;
using Ets.Group.Data.Db.EtsGroupService.DTO.ModelService;

namespace Ets.Group.Data.Db.EtsGroupService
{
    public interface IEtsGroupService
    {
        ICatalogRepository CatalogRepository { get; }
        ICatalogLevelRepository CatalogLevelRepository { get; }
        IAggregateRepository AggregateRepository { get; }
        IModelRepository ModelRepository { get; }
    }
}
