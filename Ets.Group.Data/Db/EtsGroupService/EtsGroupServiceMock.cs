﻿using Ets.Group.Data.Db.EtsGroupService.DTO.AggregateService;
using Ets.Group.Data.Db.EtsGroupService.DTO.CatalogLevelService;
using Ets.Group.Data.Db.EtsGroupService.DTO.CatalogService;
using Ets.Group.Data.Db.EtsGroupService.DTO.ModelService;

namespace Ets.Group.Data.Db.EtsGroupService
{
    public class EtsGroupServiceMock : IEtsGroupService
    {
        public EtsGroupServiceMock()
        {
            CatalogRepository = new CatalogRepositoryMock();
            CatalogLevelRepository = new CatalogLevelRepositoryMock();
            AggregateRepository = new AggregateRepositoryMock();
            ModelRepository = new ModelRepositoryMock();
        }

        public ICatalogRepository CatalogRepository { get; }

        public ICatalogLevelRepository CatalogLevelRepository { get; }

        public IAggregateRepository AggregateRepository { get; }

        public IModelRepository ModelRepository { get; }
    }
}
