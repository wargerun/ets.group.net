﻿using Ets.Group.Data;
using Ets.Group.Data.Db.Entity;
using Ets.Group.Data.Db.EtsGroupService;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Ets.Group.Tests
{
    public class TransferCatalogLevelsTests
    {
        private readonly List<CatalogLevel> ExpctedCatalogLevels = new List<CatalogLevel>
        {
            new CatalogLevel{ Id = 0, ParentId = 0, Name = "VOLVO" },
            new CatalogLevel{ Id = 1, ParentId = 0, Name = "ER" },

            new CatalogLevel{ Id = 2, ParentId = 0, Name = "КПП" },
            new CatalogLevel{ Id = 3, ParentId = 1, Name = "Двигатель" },
            new CatalogLevel{ Id = 4, ParentId = 1, Name = "КПП" },

            new CatalogLevel{ Id = 5, ParentId = 2, Name = "A365" },
            new CatalogLevel{ Id = 6, ParentId = 3, Name = "M4566" },
            new CatalogLevel{ Id = 7, ParentId = 3, Name = "FG4511" },
            new CatalogLevel{ Id = 8, ParentId = 4, Name = "T45459" },
        };

        [Fact]
        public void GetMigrationCatalogLevels_TransferCatalogLevelsFromTablesWithSaveHierarchical_PreSaveExpectedCatalogLevels()
        {
            TransferCatalogLevels transferCatalogLevels = new TransferCatalogLevels(new EtsGroupServiceMock());
            List<CatalogLevel> catalogLevels = transferCatalogLevels.GetMigrationCatalogLevels();

            foreach (CatalogLevel item in catalogLevels)
            {
                CatalogLevel expectedCatalogLevel = ExpctedCatalogLevels.Single(cl => cl.Id == item.Id);

                Assert.Equal<int>(expectedCatalogLevel.ParentId, item.ParentId);
                Assert.Equal(expectedCatalogLevel.Name, item.Name);
            }
        }
    }
}
