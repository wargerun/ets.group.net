using System;
using Xunit;
using Ets.Group.Data.Helpers;
using Ets.Group.Data.Db.Entity;
using System.IO;

namespace Ets.Group.Tests
{
    public class GenericExtensionsTests
    {
        #region MethodWith

        [Fact]
        public void With_ValueIsNotNull_ExpectedValueIsNotNull()
        {
            string expectedName = "I'm not null";
            Catalog catalog = new Catalog
            {
                Name = expectedName,
            };

            string actualName = catalog.With(c => c.Name);
            Assert.Equal(expectedName, actualName);
        }

        [Fact]
        public void With_ValueIsNull_ExpectedValueIsNull()
        {
            Catalog catalog = new Catalog();

            string actualName = catalog.With(c => c.Name);
            Assert.Null(actualName);
        } 

        #endregion

        [Fact]
        public void AssertDisposing_InvokeWriteAnyDiposingEntity_ExpectedObjectDisposedException()
        {
            Stream anyIdisposabe = new MemoryStream();

            anyIdisposabe.AssertDisposing(disposing: true);

            Assert.Throws<ObjectDisposedException>(() => anyIdisposabe.Write(new byte[1], 0, 0));
        }
    }
}
