create table CATALOGS
(
    id   INTEGER
        constraint CATALOG_pk
            primary key autoincrement,
    NAME TEXT not null
);

create table AGGREGATES
(
    id        integer
        constraint AGGREGATE_pk
            primary key autoincrement,
    NAME      TEXT not null,
    CatalogId INTEGER
        references CATALOGS
);

create table MODELS
(
    id          INTEGER
        constraint MODEL_pk
            primary key autoincrement,
    NAME        text not null,
    AggregateId int
        references AGGREGATES
);

create table CatalogLevels
(
    Id       INTEGER
        constraint CatalogLevels_pk
            primary key autoincrement,
    ParentId INTEGER,
    Name     TEXT
);


main> INSERT INTO CATALOG (NAME) VALUES ('VOLVO')
main> INSERT INTO CATALOG (NAME) VALUES ('ER')

main> INSERT INTO AGGREGATE (NAME, CATALOG_ID) VALUES ('КПП', 1)
main> INSERT INTO AGGREGATE (NAME, CATALOG_ID) VALUES ('Двигатель', 2)
main> INSERT INTO AGGREGATE (NAME, CATALOG_ID) VALUES ('КПП', 2)

main> INSERT INTO MODEL (NAME, AGGREGATE_ID) VALUES ('A365', 1)
main> INSERT INTO MODEL (NAME, AGGREGATE_ID) VALUES ('M4566', 2)
main> INSERT INTO MODEL (NAME, AGGREGATE_ID) VALUES ('FG4511', 2)
main> INSERT INTO MODEL (NAME, AGGREGATE_ID) VALUES ('T45459', 3)
